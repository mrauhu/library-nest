import { ArgsType, Field, Int } from '@nestjs/graphql';
import { CreateBookDto } from './create-book.dto';

@ArgsType()
export class CreateBookArgs extends CreateBookDto {
  @Field()
  name: string;

  @Field(() => Int)
  pageCount: number;

  @Field(() => Int)
  authorId: number;
}
