export class CreateBookDto {
  name: string;
  pageCount: number;
  authorId: number;
}
