import { Test, TestingModule } from '@nestjs/testing';
import { BooksService } from './books.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Author } from '../authors/author.model';
import { AuthorsService } from '../authors/authors.service';
import { Book } from './book.model';

describe('BooksService', () => {
  let authorService: AuthorsService;
  let bookService: BooksService;
  let authorId: number;
  let bookId: number;

  const authorObject = { name: 'Test author' };
  const bookObject = { name: 'Test book', pageCount: 1 };
  let expectedAuthor: Author;
  let expectedBook: Book;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'mysql',
          host: 'localhost',
          port: 3306,
          username: 'test',
          password: 'test',
          database: 'test',
          autoLoadEntities: true,
          logging: 'all',
        }),
        TypeOrmModule.forFeature([Author, Book]),
      ],
      providers: [AuthorsService, BooksService],
    }).compile();

    bookService = module.get<BooksService>(BooksService);
    authorService = module.get<AuthorsService>(AuthorsService);

    expectedAuthor = await authorService.create(authorObject);
    authorId = expectedAuthor.authorId;

    const book = await bookService.create({
      ...bookObject,
      authorId,
    });
    bookId = book.bookId;
    expectedBook = {
      ...book,
      author: expectedAuthor,
    };
  });

  it('should be defined', () => {
    expect(bookService).toBeDefined();
  });

  describe('findOneById()', () => {
    it('should return a book', async () => {
      const result = await bookService.findOneById(bookId);
      expect(result).toEqual(expectedBook);
    });
  });

  describe('find()', () => {
    it('should return a list of findByIdsbooks', async () => {
      const result = await bookService.find();
      console.log('result', result);
      expect(result).toEqual([expectedBook]);
    });
  });

  afterAll(async () => {
    await bookService.delete(bookId);
    await authorService.delete(authorId);
  });
});
