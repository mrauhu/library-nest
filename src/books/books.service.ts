import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { Book } from './Book.model';
import { CreateBookDto } from './dto/create-book.dto';

@Injectable()
export class BooksService {
  constructor(
    @InjectRepository(Book)
    private booksRepository: Repository<Book>,
  ) {}

  create(book: CreateBookDto) {
    return this.booksRepository.save(book);
  }

  async findOneById(bookId: number): Promise<Book | undefined> {
    return this.booksRepository.findOne(bookId, { relations: ['author'] });
  }

  async find(): Promise<Book[]> {
    return this.booksRepository.find({ relations: ['author'] });
  }

  async delete(bookId: number | number[]): Promise<DeleteResult> {
    return this.booksRepository.delete(bookId);
  }
}
