import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { ObjectType, Field, Int, ID } from '@nestjs/graphql';
import { Author } from '../authors/author.model';

@ObjectType()
@Entity('book')
export class Book {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  bookId: number;

  @Field()
  @Column()
  name: string;

  @Field(() => Int)
  @Column()
  pageCount: number;

  @Field(() => Int)
  @Column({ type: 'int', nullable: true })
  authorId: number;

  @Field(() => Author)
  @OneToOne(
    () => Author,
    author => author.authorId,
  )
  @JoinColumn({ name: 'authorId' })
  author: Author;
}
