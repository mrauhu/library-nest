import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Book } from './book.model';
import { BooksResolver } from './books.resolver';
import { BooksService } from './books.service';
import { AuthorsModule } from '../authors/authors.module';

@Module({
  imports: [TypeOrmModule.forFeature([Book]), AuthorsModule],
  providers: [BooksResolver, BooksService],
})
export class BooksModule {}
