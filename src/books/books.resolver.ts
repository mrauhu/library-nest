import {
  Args,
  Int,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { Author } from '../authors/author.model';
import { AuthorsService } from '../authors/authors.service';
import { Book } from './book.model';
import { BooksService } from './books.service';
import { CreateBookArgs } from './dto/create-book-args';

@Resolver(() => Book)
export class BooksResolver {
  constructor(
    private authorsService: AuthorsService,
    private booksService: BooksService,
  ) {}

  @Mutation(() => Book)
  async createBook(@Args() book: CreateBookArgs) {
    return this.booksService.create(book);
  }

  @Query(() => Book)
  async book(@Args('bookId', { type: () => Int }) bookId: number) {
    return this.booksService.findOneById(bookId);
  }

  @Query(() => [Book])
  async books() {
    return this.booksService.find();
  }

  @ResolveField(() => Author)
  async author(@Parent() book: Book) {
    const { authorId } = book;
    return this.authorsService.findOneById(authorId);
  }
}
