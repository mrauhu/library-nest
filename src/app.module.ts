import { join } from 'path';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GraphQLModule } from '@nestjs/graphql';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthorsModule } from './authors/authors.module';
import { BooksModule } from './books/books.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    GraphQLModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        autoSchemaFile: join(
          process.cwd(),
          configService.get('GRAPHQL_AUTO_SCHEMA_FILE', 'src/schema.graphql'),
        ),
        debug: configService.get<boolean>('GRAPHQL_DEBUG', true),
        playground: configService.get<boolean>('GRAPHQL_PLAYGROUND', true),
      }),
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.get('ORM_HOST', 'localhost'),
        port: configService.get<number>('ORM_PORT', 3306),
        username: configService.get('ORM_USERNAME', 'test'),
        password: configService.get('ORM_PASSWORD', 'test'),
        database: configService.get('ORM_DATABASE', 'test'),
        entities: [
          configService.get('ORM_ENTITIES', 'dist/**/*.module{.ts,.js}'),
        ],
        autoLoadEntities: true,
        synchronize: configService.get<boolean>('ORM_DATABASE', true),
      }),
    }),
    AuthorsModule,
    BooksModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
