import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Author } from './author.model';
import { AuthorsService } from './authors.service';

describe('AuthorsService', () => {
  let service: AuthorsService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'mysql',
          host: 'localhost',
          port: 3306,
          username: 'test',
          password: 'test',
          database: 'test',
          autoLoadEntities: true,
        }),
        TypeOrmModule.forFeature([Author]),
      ],
      providers: [AuthorsService],
    }).compile();

    service = module.get<AuthorsService>(AuthorsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create()', () => {
    it('should create an author', async () => {
      const expected = {
        name: 'Test',
      };
      const author = await service.create(expected);
      const { authorId } = author;
      delete author.authorId;
      await service.delete(authorId);

      expect(author).toStrictEqual(expected);
    });
  });
});
