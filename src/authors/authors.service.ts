import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { Author } from './author.model';
import { CreateAuthorDto } from './dto/create-author.dto';

@Injectable()
export class AuthorsService {
  constructor(
    @InjectRepository(Author)
    private authorsRepository: Repository<Author>,
  ) {}

  async create(author: CreateAuthorDto): Promise<Author> {
    return this.authorsRepository.save(author);
  }

  async findOneById(authorId: number): Promise<Author | undefined> {
    return this.authorsRepository.findOne(authorId);
  }

  async find(): Promise<Author[]> {
    return this.authorsRepository.find();
  }

  async delete(authorId: number): Promise<DeleteResult> {
    return this.authorsRepository.delete(authorId);
  }
}
