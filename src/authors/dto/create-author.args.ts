import { ArgsType, Field } from '@nestjs/graphql';
import { CreateAuthorDto } from './create-author.dto';

@ArgsType()
export class CreateAuthorArgs extends CreateAuthorDto {
  @Field()
  name: string;
}
