import { Args, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { AuthorsService } from './authors.service';
import { Author } from './author.model';
import { CreateAuthorArgs } from './dto/create-author.args';

@Resolver()
export class AuthorsResolver {
  constructor(private authorsService: AuthorsService) {}

  @Mutation(() => Author)
  createAuthor(@Args() author: CreateAuthorArgs) {
    return this.authorsService.create(author);
  }

  @Query(() => Author)
  async author(@Args('authorId', { type: () => Int }) authorId: number) {
    return this.authorsService.findOneById(authorId);
  }

  @Query(() => [Author])
  async authors() {
    return this.authorsService.find();
  }
}
