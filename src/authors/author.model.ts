import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { ObjectType, Field, ID } from '@nestjs/graphql';

@ObjectType()
@Entity('author')
export class Author {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  authorId: number;

  @Field()
  @Column()
  name: string;
}
