import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Author } from '../src/authors/author.model';
import { AuthorsModule } from '../src/authors/authors.module';
import { AuthorsService } from '../src/authors/authors.service';

const AUTHOR_NAME = 'Test E2E';

describe('Authors Resolver (e2e)', () => {
  // language=GraphQL
  const createAuthorQuery = `
    mutation {
      createAuthor(name: "${AUTHOR_NAME}") {
        name
      }
    }`;

  let app: INestApplication;
  let authorsService: AuthorsService;
  let authorId: number;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        GraphQLModule.forRoot({
          autoSchemaFile: true,
        }),
        TypeOrmModule.forRoot({
          type: 'mysql',
          host: 'localhost',
          port: 3306,
          username: 'test',
          password: 'test',
          database: 'test',
          autoLoadEntities: true,
        }),
        TypeOrmModule.forFeature([Author]),
        AuthorsModule,
      ],
      providers: [AuthorsService],
    }).compile();

    authorsService = moduleFixture.get<AuthorsService>(AuthorsService);

    app = moduleFixture.createNestApplication<NestFastifyApplication>(
      new FastifyAdapter(),
    );
    await app.init();
    await app
      .getHttpAdapter()
      .getInstance()
      .ready();
  });

  it('createAuthor()', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        operationName: null,
        query: createAuthorQuery,
      })
      .expect(({ body }) => {
        const result = body.data.createAuthor;
        authorId = result.id;
        expect(result.name).toBe(AUTHOR_NAME);
      })
      .expect(200);
  });

  afterAll(async () => {
    await authorsService.delete(authorId);
  });
});
