import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Author } from '../src/authors/author.model';
import { AuthorsModule } from '../src/authors/authors.module';
import { AuthorsService } from '../src/authors/authors.service';
import { Book } from '../src/books/book.model';
import { BooksModule } from '../src/books/books.module';
import { BooksService } from '../src/books/books.service';

const AUTHOR_NAME = 'Test E2E author';
const BOOK = {
  name: 'Test E2E book',
  pageCount: 1024,
};

describe('Books Resolver (e2e)', () => {
  let authorId: number;
  let bookId: number;
  let app: INestApplication;
  let authorsService: AuthorsService;
  let booksService: BooksService;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        GraphQLModule.forRoot({
          autoSchemaFile: true,
        }),
        TypeOrmModule.forRoot({
          type: 'mysql',
          host: 'localhost',
          port: 3306,
          username: 'test',
          password: 'test',
          database: 'test',
          autoLoadEntities: true,
        }),
        TypeOrmModule.forFeature([Author, Book]),
        AuthorsModule,
        BooksModule,
      ],
      providers: [AuthorsService],
    }).compile();

    authorsService = moduleFixture.get<AuthorsService>(AuthorsService);
    booksService = moduleFixture.get<BooksService>(BooksService);

    app = moduleFixture.createNestApplication<NestFastifyApplication>(
      new FastifyAdapter(),
    );
    await app.init();
    await app
      .getHttpAdapter()
      .getInstance()
      .ready();

    const { authorId: id } = await authorsService.create({
      name: AUTHOR_NAME,
    });
    authorId = id;
    const { bookId: bId } = await booksService.create({
      ...BOOK,
      authorId,
    });
    bookId = bId;
  });

  it('should run books query', () => {
    // language=GraphQL
    const booksQuery = `
      query {
        books {
          name
        }
      }`;

    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        operationName: null,
        query: booksQuery,
      })
      .expect(({ body }) => {
        expect(body.data).toStrictEqual({
          books: [
            {
              name: BOOK.name,
            },
          ],
        });
      })
      .expect(200);
  });

  it('should run books query with authors', () => {
    // language=GraphQL
    const booksQuery = `
      query {
        books {
          name
          author {
            name
          }
        }
      }`;

    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        operationName: null,
        query: booksQuery,
      })
      .expect(({ body }) => {
        expect(body.data).toStrictEqual({
          books: [
            {
              name: BOOK.name,
              author: {
                name: AUTHOR_NAME,
              },
            },
          ],
        });
      })
      .expect(200);
  });

  afterAll(async () => {
    await booksService.delete(bookId);
    await authorsService.delete(authorId);
  });
});
